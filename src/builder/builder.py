import base64
import os
import pathlib


class UIBuilder:
    template_path = f"{pathlib.Path(__file__).parent}/../ui"

    def render_index(self, info: dict):
        asset_path = f"{self.template_path}/index.html"
        if not os.path.exists(asset_path):
            return
        with open(asset_path, 'r') as _file:
            data = _file.read()
        return data.format_map(info)

    def web_asset(self, asset_file: str):
        asset_path = f"{self.template_path}{asset_file}"
        if not os.path.exists(asset_path):
            return
        with open(asset_path, 'rb') as _file:
            data = _file.read()
        return base64.b64encode(data).decode("utf-8")
