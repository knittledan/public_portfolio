import logging
from mimetypes import guess_type

from src.builder.builder import UIBuilder
from src.config.environment_config import EnvironmentConfig
from src.config.logging_config import LoggingConfig
from src.display_data.basic_info import info

setup = False  # type: bool
env   = None   # type: EnvironmentConfig
log   = None   # type: logging.RootLogger
ui_builder = None  # type: UIBuilder


def init_setup():
    global setup
    global env
    global log
    global ui_builder

    log = logging.getLogger()
    env = EnvironmentConfig()
    LoggingConfig(env.log_level)

    # Environment
    log.info("Setting up")
    log.info(vars(env))

    ui_builder = UIBuilder()


if setup is False:
    init_setup()
    setup = True


def render_index(event, context):
    try:
        asset = ui_builder.render_index(info)
        return dict(
            body=asset,
            statusCode=200,
            headers={'Content-Type': guess_type(".html")[0]})
    except Exception as e:
        log.exception(e)
        return dict(
            body=str(e),
            statusCode=500,
            headers={'Content-Type': 'application/json'})


def render_asset(event, context):
    try:
        asset_path = event.get("path")
        log.info(f"asset_path: {asset_path}")
        asset = ui_builder.web_asset(asset_path)
        return dict(
            body=asset,
            statusCode=200,
            isBase64Encoded=True,
            headers={'Content-Type': guess_type(asset_path)[0]})
    except Exception as e:
        log.exception(e)
        return dict(
            body=str(e),
            statusCode=500,
            headers={'Content-Type': 'application/json'})
