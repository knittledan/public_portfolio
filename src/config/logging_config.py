import logging


class LoggingConfig:
    log_string_format = '[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s'

    def __init__(self, log_level=None):
        log_level = self.lookup_log_level(log_level)
        LoggingConfig.setup_stream_logging(log_level)
        logging.getLogger('').setLevel(logging.INFO)

    @staticmethod
    def setup_stream_logging(log_level):
        strhdlr = logging.StreamHandler()
        strhdlr.setLevel(log_level)
        logging.basicConfig(format=LoggingConfig.log_string_format, level=log_level, handlers=[strhdlr])
        logging.getLogger('').log(log_level, "Logging file startup")

    @staticmethod
    def setup_formatter(handler):
        formatter = logging.Formatter(LoggingConfig.log_string_format)
        handler.setFormatter(formatter)

    @staticmethod
    def lookup_log_level(log_level):
        lookup = {'info': logging.INFO, 'debug': logging.DEBUG, 'error': logging.ERROR,
                  'critical': logging.CRITICAL, 'warn': logging.WARN, 'fatal': logging.FATAL}
        return lookup.get(log_level.lower(), logging.ERROR)

