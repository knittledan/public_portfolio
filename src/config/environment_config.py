import os


class EnvironmentConfig:
    """
    Environment Config
    """
    log_level = 'INFO'

    def __init__(self):
        # Logging
        self.log_level = os.getenv('LOG_LEVEL', self.log_level)
