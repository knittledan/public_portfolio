from src.display_data.descriptions import description_1, description_2, about_me

info = {
    "browser title": "Dan Knittle's Portfolio",
    "author": "Dan Knittle",
    "keywords": "Engineering, Python, NodeJS, Kotlin, AWS, An awesome dude",
    "first": "Dan",
    "last": "Knittle",
    "experience": "7 Years",
    "residence": "USA",
    "e-mail": "knittledan@gmail.com",
    "address": "San Diego, CA",
    "relocation": "Yes, considered",
    "resume link": "https://docs.google.com/document/d/1NnlSi_cIQBgBUmWDTHxAHCKHWga0aXGxlD8dNNjuQxw/edit?usp=sharing",
    "about": about_me,
    "languages": "Python, NodeJS, Kotlin, Java, PHP, Shell Scripting, Some exposure to GoLang",
    "aws": "ECS, Api Gateway, Lambda, ElastiCache, RDS, SQS/SNS, S3, CloudFormation, VPC/Subnet, ELBs, Route53",
    "orchestration": "Kubernetes, Docker, Hashicorp Terraform, Serverless, Jenkins",
    "databases": "MongoDB Enterprise, MySql, AWS RDS, DynamoDB, Redis, Prometheus, Grafana",
    "experience_1": {
        "title": "Principal Software Engineer",
        "employment": "Mar 2015 - Current",
        "company": "Midigator LLC",
        "description": description_1
    },
    "experience_2": {
        "title": "Engineer, Flix",
        "employment": "May 2013 - Mar 2015",
        "company": "The Foundry",
        "description": description_2
    },
    "experience_3": {
        "title": "Engineer, Support",
        "employment": "Oct 2011 - May 2013",
        "company": "The Foundry",
        "description": description_2
    },
    "education_1": {
        "title": "Bachelors of Computer Science",
        "employment": "2011, Graduated",
        "company": "Art Institute of California"
    },
    "education_2": {
        "title": "Object Oriented Programming",
        "employment": "2013, 3 Credit Course",
        "company": "UCLA Extension"
    },
    "education_3": {
        "title": "Business Intelligence & Data Analytics Overview",
        "employment": "2017, 3 Credit Course",
        "company": "UCSD Extension"
    }
}