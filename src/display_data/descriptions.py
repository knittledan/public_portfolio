about_me = """
Dan is a passionate software engineer with 7 years experience in the field. Known for pushing the hardest of projects 
through to the finish line. Excellent at identifying company bottlenecks through familiarizing himself with current 
difficulties from other departments and end-users. He's successful in leading 
technical overhauls and integrating new technology by creating proof of concepts, presenting the pros and cons, and 
training the teams. His failures are quickly realized and corrected. He enjoys designing and building the frameworks 
that will be relied upon for the companies growth. To him, the code is an art and should be aesthetically pleasing 
while keeping it simple and functional. He's strong in many programming languages, infrastructure, orchestration, 
restful APIs, data streaming, metrics, cloud providers, and debugging. He will delivery production stable services from 
the ground up.
"""

description_1 = """
- Introduced and delivered technology which allowed the company to scale from our initial few clients to over a thousand clients.
<br>
- Developed our data ingestion pipeline to extract data from banks, processors, and transaction management systems into a NoSQL database to then be normalized into RDS SQL Database.
<br>
- Continuously developed critical services that have resulted in the outstanding growth of the company.
<br>
- Built communication services such as sms, email, and fax which automated processes between the banks, customers,
and merchants.
<br>
- Lead the migration from self managed data-centers into aws.
<br>
- Introduced and fully integrated deployment orchestration with Terraform and Serverless.
<br>
- Converted all legacy projects into docker containers while teaching the teams how to use said technology.
<br>
- Created the framework and infrastructure for micro-services to be deployed in AWS as containers.
<br>
- Worked closely with stakeholders, partners, and internal departments to understand and capture project requirements
while communicating progress.
<br>
- Regularly identified internal bottlenecks and frustrations then delivered the solutions.
<br>
- Created our internal and customer facing REST API.
<br>
- Proficient with Selenium and Puppeteer
"""

description_2 = """
- Responsible for delivering and maintaining releases of Flix to large scale animation studios.
<br>
- Developed feature that enabled directors and artist to provide live feedback and edits to their scenes.
<br>
- Architected the infrastructure which Flix ran on within each studio while meeting individual compliance practices.
<br>
- Designed the way we package and distributed the product which was a web based cloud experience.
<br>
- Maintained client support and customization, including on-site visits and sales demos. Provided support to numerous
Animation studios such as DreamWorks, Paramount, EA Sports, Warner Bros, Sony, and Laika.
"""

description_3 = """
- Developed internal tools which measurably improved the Support Departments efficiency by monitoring and parsing all communication. Created a web interface to provide visual feedback, analytics, and more.
<br>
- Integrated analytics with SalesForce, Zendesk, and MySQL.
<br>
- Provided film studios with dedicated support, code fixes, and training for our suite of products’ SDK and Python
"""